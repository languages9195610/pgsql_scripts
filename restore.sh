#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

source ./.env
source ./.helpers

if [ -f "$FILELOCK" ]; then
    printf "%s exists! Remove it and try again" "$FILELOCK"
    exit 191
else
    touch "$FILELOCK"
fi

DB_ARCHIVE_FULL_PATH=$1
DB=$2

for i in "$DB" "$DB_ARCHIVE_FULL_PATH"; do
    checkVar "$i"; done

set -o nounset

DB_NEW="$(printf "%s" "$(/usr/bin/psql -t -U "$PGUSER" --command "SELECT datname FROM pg_database WHERE datname='$DB';" | sed 's/[[:space:]]*//g')")"

if [ "$DB_NEW" == "$DB" ]; then
    printf "%s name is existed\n" "$DB_NEW"
    rm -f "$FILELOCK"
    exit 199
else
    /usr/bin/createdb --encoding=UTF-8 --locale='ru_RU.UTF-8' -T template0 "$DB"
    gunzip -c "$DB_ARCHIVE_FULL_PATH" | /usr/bin/psql --echo-errors --quiet --set='ON_ERROR_STOP=on' --dbname=postgresql://"$PGUSER":"$PGPASSWORD"@"$PGHOST"/"$DB"
fi

rm -f "$FILELOCK"
exit 0

