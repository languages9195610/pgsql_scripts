#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

source ./.env

# if [ -f "$FILELOCK" ]; then
#     printf "%s exists! Remove it and try again" "$FILELOCK"
#     exit 191
# else
#     touch "$FILELOCK"
# fi

if [ "$(date +%w)" -eq 5 ]; then 
    BACKUP_DIR="$BACKUP_ROOT/weekly/$(date +%F)"
else
    BACKUP_DIR="$BACKUP_ROOT/daily/$(date +%F)"
fi

mkdir -p "$BACKUP_DIR"
mkdir -p "$BACKUP_ROOT/logs"

DB_LIST=$(/usr/bin/psql -l -t -U "$PGUSER" |  /usr/bin/cut -d'|' -f1 | sed '/^ *$/d' | grep -Eiv "$EXCLUDE")

for DB in $DB_LIST; do 
    LOGFILE="$BACKUP_ROOT/logs/$DB.$(date +%Y-%m-%d--%H-%m-%S).$$.log"
    echo "$DB"
    time (/usr/bin/pg_dump --no-password --quote-all-identifiers --format=plain --dbname=postgresql://"$PGUSER":"$PGPASSWORD"@"$PGHOST"/"$DB" | /bin/gzip -c > "$BACKUP_DIR/$DB--$(date +%F--%H-%M).sql.gz") > "$LOGFILE" 2>&1 || echo "!!! There was en error!" | tee --append "$LOGFILE"; done

# rm -f "$FILELOCK"
exit 0

